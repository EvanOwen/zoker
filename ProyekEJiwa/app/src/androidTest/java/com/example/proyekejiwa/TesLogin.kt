package com.example.proyekejiwa

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {
    @Rule
    @JvmField
    //inisialisasi activity mana yang akan dilakukan tes
    var activityRule = ActivityTestRule<LoginActivity>(
        LoginActivity::class.java
    )
    //Tes terhadap password dan username salah
    //inisialisasi username dan password yang akan dites
    private val usernamesalah2 = "Username salah"
    private val passwordsalah2 = "Password salah"

    @Test
    fun clickLoginButtonWrongBoth() {
        //isi username
        onView(withId(R.id.tvUserName)).perform(ViewActions.typeText(usernamesalah2))
        //isi password
        onView(withId(R.id.tvPassword)).perform(ViewActions.typeText(passwordsalah2))
        //setelah selesai mengisi password virtual keyboard di close agar tidak menghalangi button saat mau diklik
        onView(withId(R.id.tvPassword)).perform(closeSoftKeyboard());
        //melaukan klik pada button SignIn
        onView(withId(R.id.btnLoginSignIn)).perform(click())
        //mengecek apakah output sesuai dengan yagn diinginkan
        onView(withId(R.id.tvWrongLogin))
            .check(matches(withText("Username dan Password Salah")))
    }

    //Tes terhadap password salah dan username benar
    //inisialisasi username dan password yang akan dites
    private val usernamebenar = "zoker"
    private val passwordsalah = "passwordsalah"

    @Test
    fun clickLoginButtonWrongPass() {
        //isi username
        onView(withId(R.id.tvUserName)).perform(ViewActions.typeText(usernamebenar))
        //isi password
        onView(withId(R.id.tvPassword)).perform(ViewActions.typeText(passwordsalah))
        //setelah selesai mengisi password virtual keyboard di close agar tidak menghalangi button saat mau diklik
        onView(withId(R.id.tvPassword)).perform(closeSoftKeyboard());
        //melaukan klik pada button SignIn
        onView(withId(R.id.btnLoginSignIn)).perform(click())
        //mengecek apakah output sesuai dengan yagn diinginkan
        onView(withId(R.id.tvWrongLogin))
            .check(matches(withText("Password Salah")))
    }

    //Tes terhadap password benar dan username salah
    //inisialisasi username dan password yang akan dites
    private val usernamesalah = "Username salah"
    private val passwordbenar = "admin"

    @Test
    fun clickLoginButtonWrongUser() {
        //isi username
        onView(withId(R.id.tvUserName)).perform(ViewActions.typeText(usernamesalah))
        //isi password
        onView(withId(R.id.tvPassword)).perform(ViewActions.typeText(passwordbenar))
        //setelah selesai mengisi password virtual keyboard di close agar tidak menghalangi button saat mau diklik
        onView(withId(R.id.tvPassword)).perform(closeSoftKeyboard());
        //melaukan klik pada button SignIn
        onView(withId(R.id.btnLoginSignIn)).perform(click())
        //mengecek apakah output sesuai dengan yagn diinginkan
        onView(withId(R.id.tvWrongLogin))
            .check(matches(withText("Username Salah")))
    }

    //Tes terhadap password atau username dalam keadaan tidak diisi.
    //inisialisasi username dan password yang akan dites
    private val usernamekosong = ""
    private val passwordkosong = ""

    @Test
    fun clickLoginButtonNull() {
        //isi username
        onView(withId(R.id.tvUserName)).perform(ViewActions.typeText(usernamekosong))
        //isi password
        onView(withId(R.id.tvPassword)).perform(ViewActions.typeText(passwordkosong))
        //setelah selesai mengisi password virtual keyboard di close agar tidak menghalangi button saat mau diklik
        onView(withId(R.id.tvPassword)).perform(closeSoftKeyboard());
        //melaukan klik pada button SignIn
        onView(withId(R.id.btnLoginSignIn)).perform(click())
        //mengecek apakah output sesuai dengan yagn diinginkan
        onView(withId(R.id.tvWrongLogin))
            .check(matches(withText("Username dan Password Harus Diisi")))
   }
}