package com.example.proyekejiwa

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LayananButtonTest {
    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun clickMenuButton() {
        //melakukan pengecekan apakah button dengan id tersebut ditampilkan.
        Espresso.onView(ViewMatchers.withId(R.id.btn_konsultasi)).check(
            ViewAssertions.matches(
                ViewMatchers.isDisplayed()
            )
        )
        //melakukan klik pada button.
        Espresso.onView(ViewMatchers.withId(R.id.btn_konsultasi)).perform(ViewActions.click())
        //melakukan pengecekan apakan tampilah sesuai dengan yang diminta.
        Espresso.onView(ViewMatchers.withId(R.id.tvJudulLayanan)).check(
            ViewAssertions.matches(
                ViewMatchers.isDisplayed()
            )
        )
    }
}