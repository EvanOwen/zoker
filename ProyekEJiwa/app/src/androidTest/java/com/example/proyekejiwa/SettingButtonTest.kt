package com.example.proyekejiwa

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @Rule
    @JvmField
    var activityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun clickMenuButton() {
        //Check Apakah button dengan id settingNotfi ada
        Espresso.onView(withId(R.id.settingNotif)).check(matches(isDisplayed()))
        //lakukan klik pada button
        Espresso.onView(withId(R.id.settingNotif)).perform(ViewActions.click())
        //check apakah menampilkan halaman yang diinginkan
        Espresso.onView(withId(R.id.tvPengggunaanApp)).check(matches(isDisplayed()))
    }
}