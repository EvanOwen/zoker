package com.example.proyekejiwa.widget
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import com.example.proyekejiwa.R
import com.example.proyekejiwa.notification.AlarmReceiver
import com.example.proyekejiwa.notification.SettingNotificationActivity
import com.example.proyekejiwa.sharedpreference.SharePrefHelper

/**
 * Implementation of App Widget functionality.
 */
class AlarmWidget : AppWidgetProvider() {
    lateinit var alarmReceiver: AlarmReceiver
    //proses update widget
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        //Mungkin ada banyak widget active, sehingga dibutuhkan perulangan.
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(
                context,
                appWidgetManager,
                appWidgetId
            )
        }
    }
    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        //ketika button pada widget diclik
        if(stopOnClick.equals(intent?.action)){
            //mengambil shared preference dengan key PREFERENCEKEY
            val sharedPref= SharePrefHelper(context as Context, "PREFERENCEKEY");
            alarmReceiver = AlarmReceiver()
            alarmReceiver.cancelAlarm(context);
            sharedPref.clearValues() //Menghapus semua nilai sharedPref
            sharedPref.time = "time";
            val appWidgetManager = AppWidgetManager.getInstance(context)
            val thisAppWidgetComponenName = ComponentName(context!!.packageName, javaClass.name)
            val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidgetComponenName)
            //melakukan update.
            for(appWidgetId in appWidgetIds){
                updateAppWidget(context as Context, appWidgetManager,appWidgetId)
            }
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }
    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    companion object {
        private val stopOnClick = "StopOnClick" //key untuk button
        //fungsi untuk melakukan pengupdatean
        internal fun updateAppWidget(
            context: Context,
            appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            //mengambil shared preference dengan key PREFERENCEKEY
            val sharedPref = SharePrefHelper(context as Context, "PREFERENCEKEY");
            var text = "";
            if (sharedPref.time == "time") { //jika sharedPref.time tidak di setting/default;
                text = "Notifikasi Belum Diatur"
            } else { //ketika sudah diatur.
                text = sharedPref.time
            }
            //menyimpan text pada variabel.
            val widgetText = text
            //mengambil view pada widget.
            val views = RemoteViews(
                context.packageName,
                R.layout.alarm_widget
            )
            //Mengatur text / mengubah text
            views.setTextViewText(R.id.appwidget_text, widgetText)
            //event ketika button pada widget di klik
            views.setOnClickPendingIntent(
                R.id.btn_stop_alarm_widget,
                getPendingSelfIntent(context, stopOnClick)
            )
            //event ketika widget di klik
            views.setOnClickPendingIntent(R.id.widget_layout, getPendingIntent(context))
            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }

        //membentuk broadcast ke diri sendiri
        private fun getPendingSelfIntent(context: Context, buttonStr: String): PendingIntent{
            return Intent(context, AlarmWidget::class.java).let{
                it.action = buttonStr
                PendingIntent.getBroadcast(context,11,it,
                    PendingIntent.FLAG_CANCEL_CURRENT)
            }
        }
        //membuat broadcast ke Aktivity
        private fun getPendingIntent(context: Context): PendingIntent{
            return Intent(context, SettingNotificationActivity::class.java).let{
                PendingIntent.getActivity(context,0 ,it,
                    PendingIntent.FLAG_CANCEL_CURRENT)
            }
        }
    }
}

