package com.example.proyekejiwa

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.login_layout.*
import org.w3c.dom.Text
import java.util.zip.Inflater

class homeFragment : Fragment(), View.OnClickListener {

    companion object {
        var REQUEST_REPLY = 100
        var EXTRA_REPLAY = "replay"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvNameUserAfterLogin.visibility = View.GONE
        tvHalo.visibility = View.GONE

        //tvNameUserAfterLogin.text = "Zoker"
        //tvCheckLogin.visibility = View.GONE

        tvSignInBeforeLogin.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        if(v?.id == R.id.tvSignInBeforeLogin) {
            var intent = Intent(activity, LoginActivity::class.java)
            startActivityForResult(intent, REQUEST_REPLY)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode ==  REQUEST_REPLY &&
            resultCode == Activity.RESULT_OK &&
            data != null){
            if(data.hasExtra(EXTRA_REPLAY)){
                tvCheckLogin.visibility = View.GONE
                tvNameUserAfterLogin.text = data.getStringExtra(EXTRA_REPLAY)
                tvNameUserAfterLogin.visibility = View.VISIBLE
                tvHalo.visibility = View.VISIBLE
            }
        }
    }
}
