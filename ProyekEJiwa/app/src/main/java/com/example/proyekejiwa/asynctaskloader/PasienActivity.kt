package com.example.proyekejiwa.asynctaskloader

import android.content.DialogInterface
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proyekejiwa.R
import com.example.proyekejiwa.asynctaskloader.db.myDBHelper
import kotlinx.android.synthetic.main.activity_pasien.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

@Suppress("DEPRECATION")
class PasienActivity : AppCompatActivity(), BackgroundProgressInterface
    ,LoaderManager.LoaderCallbacks<ArrayList<Pasien>>,
    View.OnClickListener //Load manager dengan parameter
                        // ArrayList<Pasien> agar nanti menghasilkan sebuah list.
{
    val LOADER_ID = 25
    val bundleValue: String = "1,000"
    var progressTxtView: TextView? = null
    val bundle = Bundle()
    //inisialisasi Recycle View
    private lateinit var rviewPasien : RecyclerView
    var sizeFile = 0;

    companion object {
        val TAG = "PasienActivity"
        val BUNDLE_KEY = "key.to.identify.bundle.value"
    }

    var mysqlitedb: myDBHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pasien)
        //buat di variabel agar terbaca di onUpdateProgress
        progressTxtView = findViewById(R.id.progress)
        //agar terlihat saat diulang kembali.
        progressTxtView?.visibility = View.VISIBLE
        bundle.putString(BUNDLE_KEY, bundleValue)  //meletakkan nilai bundle
        supportLoaderManager.initLoader(LOADER_ID, null, this) //init loader
        makeOperationAddNumber() //Memanggil fungsi agar proses AsyncTaskLoader berjalan.

        //SQLITE
        if (mysqlitedb == null) {
//            progressTxtView?.visibility = View.GONE
            mysqlitedb =
                myDBHelper(this) //untuk membuat database, anda cukup memanggil perintah ini
        }

        btnAddPasien.setOnClickListener(this)
    }

    //Pembuatan loader return hasil dari AsycTaskLoader
    override fun onCreateLoader(id: Int, args: Bundle?): Loader<ArrayList<Pasien>> {
        return AsyncTaskLoaderSubClass(this, args, this, mysqlitedb)
    }

    //proses yang melakungan pengupdate pada progress bar. diambil dari Interface
    override fun onUpdateProgress(progress: Int) {
        runOnUiThread(Runnable {
            // This will run on the UI thread
            kotlin.run {
                progressTxtView?.setText("Progress = $progress%")  //update Text.
            }
        })
    }

    private fun makeOperationAddNumber() {
        // this will try to fetch a Loader with ID = LOADER_ID
        val loader: Loader<Long>? = supportLoaderManager.getLoader(LOADER_ID)
        if (loader == null) {
            /* jika Loader dengan loaderID tidak ditemukan,
             Inisialisasi Loader Baru dengan ID = LOADER_ID
             Masukkan bundel yang akan digunakan AsynTaskLoader
             Juga berikan callback yang diperlukan yaitu 'this' karena kami telah menerapkannya pada activity
             */
            supportLoaderManager.initLoader(LOADER_ID, bundle, this)
        } else {
            /* jika Loader dengan loaderID tidak ditemukan,
             Inisialisasi Loader Baru dengan ID = LOADER_ID
             Masukkan bundel yang akan digunakan AsynTaskLoader
             Juga berikan callback yang diperlukan yaitu 'this' karena kami telah menerapkannya pada activity
             */
            supportLoaderManager.restartLoader(LOADER_ID, bundle, this)
        }
    }
    //Saat proses pengambilan data selesai
    override fun onLoadFinished(loader: Loader<ArrayList<Pasien>>, data: ArrayList<Pasien>?) {
        //jika data kosong maka tidak mengeluarkan output apa apa.
        if (data == null || data.count() < 1) {
            progressTxtView?.visibility = View.GONE
            return
        } else {
            //menyembunyikan progress bar.
            progressTxtView?.visibility = View.GONE
            //menghasilkan output dalam bentuk recyclerView.
            rviewPasien = findViewById(R.id.rvPasien)
            rviewPasien.setHasFixedSize(true)
            rviewPasien.layoutManager = LinearLayoutManager(this)
            //memasukkan data ke adapter sehingga menghasilkan output.
            var pasienAdapter = PasienAdapter(this,data)
            rviewPasien.adapter = pasienAdapter

            //Tombol untuk menghapus data, dengan memanfaatkan interface dari pasien adapter
            pasienAdapter.setOnItemDeletedCallback(object : PasienAdapter.deleteItem {
                override fun onItemDeleted(id: Int) {
                    deleteData(id) //memanggil fungsi deleteData
                }
            })
            //Tombol untuk menghapus data, dengan memanfaatkan interface dari pasien adapter
            pasienAdapter.setOnItemUpdatedCallback(object : PasienAdapter.updateItem {
                override fun onItemUpdated(id: Int, pasien: Pasien) {
                    updateData(id, pasien) //memanggil fungsi updateData
                }
            })

        }
    }
    override fun onLoaderReset(loader: Loader<ArrayList<Pasien>>) {}

    override fun onClick(v: View?) {
        //dialog untuk memambah data.
        var dialogBuilt = AlertDialog.Builder(this)
        //layout untuk menambah data.
        var layoutAlert = layoutInflater.inflate(R.layout.dialog_add_pasien_layout, null)
        var jenis_kelamin = ""
        //deklarasi semua view.
        val nama = layoutAlert.findViewById<EditText>(R.id.tvNamaPasien)
        val jenis_kelamin_Lk = layoutAlert.findViewById<RadioButton>(R.id.rbJKLk)
        val umur = layoutAlert.findViewById<EditText>(R.id.tvUmurPasien)
        val alamat = layoutAlert.findViewById<EditText>(R.id.tvAlamatPasien)
        val telepon = layoutAlert.findViewById<EditText>(R.id.tvTeleponPasien)
        val checkSize =layoutAlert.findViewById<TextView>(R.id.tvCheckSize)

        //Mengecek Size dari inputan baru yang akan dimasukkan ke penyimpanan external.
        checkSize.setOnClickListener {
            if(jenis_kelamin_Lk.isChecked) {
                jenis_kelamin = "Laki-laki"
            }else {
                jenis_kelamin = "Perempuan"
            }
            val size = nama.length() + jenis_kelamin.length + umur.length() + alamat.length() +
                    telepon.length() + 10
            val alertDialogSize = AlertDialog.Builder(this)
            val layoutSize = layoutInflater.inflate(R.layout.dialog_size_layout, null)
            val textSize = layoutSize.findViewById<TextView>(R.id.tvSizeFileNew)
            textSize.text = size.toString() + " Byte"
            alertDialogSize.setNegativeButton("OK", object:DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {}
            })
            alertDialogSize.setTitle("Size New Input")
            alertDialogSize.setView(layoutSize)
            val dialogsize = alertDialogSize.create()
            dialogsize.show() //menampilkan dialog
        }
        //mengatur judul dan layout dari dialog
        dialogBuilt.setTitle("Input Data Pasien")
        dialogBuilt.setView(layoutAlert)

        //mengatur positive button.
        dialogBuilt.setPositiveButton("Tambah", object:DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if(jenis_kelamin_Lk.isChecked) {
                    jenis_kelamin = "Laki-laki"
                }else {
                    jenis_kelamin = "Perempuan"
                }
                //Save data ke database
                saveData(nama.text.toString(), umur.text.toString().toInt(),alamat.text.toString(),
                    jenis_kelamin, telepon.text.toString())
                recreate() //refress activity
            }
        })
        //pengaturan Negative Button.
        dialogBuilt.setNegativeButton("Batal", object :DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
            }
        })
        val dialog = dialogBuilt.create()
        dialog.show() //Menampilkan dialog.
    }

    //Fungsi untuk menyimpan data ke database.
    fun saveData(nama: String, umur:Int, alamat:String, jenisKelamin:String, telepon:String) {
        val pasien = Pasien(0,nama,jenisKelamin, umur, alamat, telepon);

        val result = mysqlitedb?.addPasien(pasien) //pemanggilan fungsi untuk insert data
        if (result != -1L) //ingat, data yang dikembalikan, jika -1, maka data gagal disimpan
            Toast.makeText(this, "Tersimpan Ke Database", Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
    }

    //funsi untuk melakukan delete terhadap data berdasarkan id
    fun deleteData(id : Int) {
        //memanggil fungsi delete paaa myDBHelper.kt
        mysqlitedb?.deleteById(id)
        recreate() //refresh activity
    }

    fun updateData(id : Int, pasien : Pasien) {
        //dialog untuk mengupdate data.
        val dialogBuilt = AlertDialog.Builder(this)
        //layout untuk mengupdate data.
        val layoutAlert = layoutInflater.inflate(R.layout.dialog_add_pasien_layout, null)
        var jenis_kelamin = ""
        //deklarasi semua view dari layoutAlert
        val nama = layoutAlert.findViewById<EditText>(R.id.tvNamaPasien)
        val jenis_kelamin_Lk = layoutAlert.findViewById<RadioButton>(R.id.rbJKLk)
        val jenis_kelamin_Pr = layoutAlert.findViewById<RadioButton>(R.id.rbJKPr)
        val umur = layoutAlert.findViewById<EditText>(R.id.tvUmurPasien)
        val alamat = layoutAlert.findViewById<EditText>(R.id.tvAlamatPasien)
        val telepon = layoutAlert.findViewById<EditText>(R.id.tvTeleponPasien)
        val checkSize =layoutAlert.findViewById<TextView>(R.id.tvCheckSize)
        checkSize.visibility = View.GONE

        nama.setText(pasien.nama)
        if(pasien.jenisKelamin == "Laki-laki") jenis_kelamin_Lk.isChecked = true
        else jenis_kelamin_Pr.isChecked = true
        umur.setText(pasien.umur.toString())
        alamat.setText(pasien.alamat)
        telepon.setText(pasien.noHp)

        //mengatur judul dan layout dari dialog
        dialogBuilt.setTitle("Update Data Pasien")
        dialogBuilt.setView(layoutAlert)

        //mengatur positive button.
        dialogBuilt.setPositiveButton("Update", object:DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if(jenis_kelamin_Lk.isChecked) {
                    jenis_kelamin = "Laki-laki"
                }else {
                    jenis_kelamin = "Perempuan"
                }
                //menyimpan data dalam bentuk data class yang akan dikirim ke myDBHelper.
                val pasienTemp = Pasien(
                    0,
                    nama.text.toString(),
                    jenis_kelamin,
                    umur.text.toString().toInt(),
                    alamat.text.toString(),
                    telepon.text.toString()
                )
                //melakukan operasi update data ke database berdasarkan id
                mysqlitedb?.updateById(id, pasienTemp)
                recreate() //refress activity
            }
        })
        //pengaturan Negative Button.
        dialogBuilt.setNegativeButton("Batal", object :DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
            }
        })
        val dialog = dialogBuilt.create()
        dialog.show() //Menampilkan dialog.

    }
}