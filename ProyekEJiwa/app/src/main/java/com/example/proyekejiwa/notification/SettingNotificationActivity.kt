package com.example.proyekejiwa.notification

import android.app.TimePickerDialog
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color.BLACK
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.proyekejiwa.R
import com.example.proyekejiwa.sharedpreference.SharePrefHelper
import com.example.proyekejiwa.widget.AlarmWidget
import kotlinx.android.synthetic.main.activity_setting_notification.*
import java.util.*
import java.util.prefs.Preferences

//deklarasi key dari sharePreference yang akan dikirim ke class SharePrefHelper
var PrefName = "PREFERENCEKEY"
class SettingNotificationActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var alarmReceiver: AlarmReceiver  //deklarasi AlarmReceiver agar terjadi perulangan waktu
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_notification)
        swNotif.setOnClickListener(this) //saat switch di klik
        btn_time.setOnClickListener(this) //saat tombol waktu ditekan
        alarmReceiver = AlarmReceiver()  //inisialisasi alarmReceiver
        btn_time.visibility = View.GONE
        btn_time.isEnabled = false;
        btn_time.setTextColor(BLACK)
        btn_time.setText("") //mengatur teks waktu mula mula.

        //Mengambil nilai yang tersimpan di SharePreference
        val status = SharePrefHelper(this, PrefName).status //mengambil nilai status
        val time = SharePrefHelper(this, PrefName).time  //mengambil nilai waktu
        //jika status dalam keadaan true;
        if(status) {
            swNotif.isChecked = true //mengubah switch menjadi on
            btn_time.visibility = View.VISIBLE //menampilkan waktu
            btn_time.setText(time); //mengatur text waktu dimana text diambil dari SharePreference dengan key time
            swNotif.text = "Pengingat Di Atur Di Jam ${btn_time.text}"
        }
    }

    override fun onClick(v: View) {
        //saat tombol switch di klik
        if(v.id == R.id.swNotif) {
            //Deklarasi variabel class sharePrefHelper
            val sharePrefHelper = SharePrefHelper(this, PrefName)
            if(swNotif.isChecked) {
                val now = Calendar.getInstance() //mengambil waktu sekarang
                val jam = now.get(Calendar.HOUR) //mengambil jam sekarang
                val menit = now.get(Calendar.MINUTE) //mengambil menit sekarang

                //untuk mengatur waktu yang akan digunakan pada notifikasi.
                val myTimePicker = TimePickerDialog(this,
                    TimePickerDialog
                        .OnTimeSetListener { datePicker, hour, minutes->
                            run {
                                btn_time.text = hour.toString().padStart(2, '0') +
                                        ":" + minutes.toString().padStart(2, '0')
                                swNotif.text =
                                    "Pengingat Di Atur Di Jam ${btn_time.text}" //mengatur teks ketika ditekan oke
                                                                                // pada picker
                                val repeatTime =
                                    btn_time.text.toString()  //menyimpan waktu dalam string
                                alarmReceiver.setRepeatingAlarm(
                                    this,
                                    repeatTime
                                ) //Melakukan pemanggilan Receiver untuk Melakukan setting waktu
                                //pemanggilan notifikasi
                                btn_time.visibility = View.VISIBLE

                                //Menyimpan nilai ke SharePreference
                                sharePrefHelper.status = true;
                                sharePrefHelper.time = btn_time.text.toString()
                            }
                        },jam,menit,true)
                //Ketika Ditekan Cancel pada Time Picker Dialog maka switch akan dikembalikan seperti semula.
                myTimePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",object :DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        Toast.makeText(this@SettingNotificationActivity, "Cancel", Toast.LENGTH_LONG).show()
                        //Mengembalikan switch ke semula atau kembali off.
                        swNotif.isChecked = false;
                    }
                })
                //Menampilkan Time Picker Dialog
                myTimePicker.show()
            }
            else {
                //jika switch off maka sharePreference diubah menjadi default
                sharePrefHelper.clearValues()
                sharePrefHelper.time = "time";
                btn_time.visibility = View.GONE
                swNotif.text = "Pengingat Tidak Aktif" //ketika switch mati, teks berganti.
                alarmReceiver.cancelAlarm(this) //melakukan cancel pada receiver, agar tidak terjadi
                                                        // tejadi pemanggilan notifikasi lagi.
            }
        }
    }

    //diperlukan agar data dengan widget sama.
    override fun onResume() {
        super.onResume()
        //Mengambil nilai yang tersimpan di SharePreference
        val status = SharePrefHelper(this, PrefName).status //mengambil nilai status
        val time = SharePrefHelper(this, PrefName).time  //mengambil nilai waktu
        //jika status dalam keadaan true;
        if(status) {
            swNotif.isChecked = true //mengubah switch menjadi on
            btn_time.visibility = View.VISIBLE //menampilkan waktu
            btn_time.setText(time); //mengatur text waktu dimana text diambil dari SharePreference dengan key time
            swNotif.text = "Pengingat Di Atur Di Jam ${btn_time.text}"
        }else {
            swNotif.isChecked = false//mengubah switch menjadi on
            btn_time.visibility = View.GONE //menampilkan waktu
        }
    }
    //digunakan ketika aplikasi telah berhenti
    //activity akan mengirimkan broadcast untuk segera mengupdate isi atau layout dari widget
    override fun onStop() {
        super.onStop()
        val appWidgetManager = AppWidgetManager.getInstance(this)
        val ids = appWidgetManager.getAppWidgetIds(ComponentName(this, AlarmWidget::class.java))
        val updateIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE)
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids)
        sendBroadcast(updateIntent)
    }
}