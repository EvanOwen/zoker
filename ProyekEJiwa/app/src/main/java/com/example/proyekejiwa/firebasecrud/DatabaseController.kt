package com.example.proyekejiwa.firebasecrud

import android.content.Context
import android.widget.Toast
import com.google.firebase.database.*
import kotlin.collections.MutableMap as MutableMap1

class DatabaseController(activityContext : Context){
    //memdeklarasikan akses ke firebase database
    private lateinit var ref : DatabaseReference;
    //lebih baik jika dijadikan kelas Data (MVP/MVC) daripada digabungkan di controller
    private var dataTmp = readData()
    private var context = activityContext;
    fun saveData(dataDokter : Dokter) {
        //Inisialiasi firebase database dan membuka path baru, dengan induk bernama USERS
        ref = FirebaseDatabase.getInstance().getReference("DOCTERS")
        //membentuk key unik untuk ID dalam penyimpanan database
        val DokterID = ref.push().key.toString()
        //membuat value untuk key untuk membentuk json objek dengan isi userData
        ref.child(DokterID).setValue(dataDokter).apply {
            addOnCompleteListener {//dijalankan jika penyimpanan data berhasil
                Toast.makeText(context,"Data Tersimpan", Toast.LENGTH_SHORT).show()
            }
            addOnFailureListener {//dijalankan jika penyimpanan data gagal
                Toast.makeText(context, "${it.message}", Toast.LENGTH_SHORT).show()
            }
            addOnCanceledListener {  }
            addOnSuccessListener {  }
        }
    }
    //fungsi untuk melakukan pembacaan atau pengambilan data dari firebase.
    fun readData() : MutableMap1<String,Int> {
        val hasil = mutableMapOf<String,Int>()
        //Inisialiasi firebase database dan membuka path baru, dengan induk bernama USERS
        ref = FirebaseDatabase.getInstance().getReference("DOCTERS")
        //fungsi untuk membaca data
        ref.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented")
            }
            override fun onDataChange(p0: DataSnapshot) {
                //memastikan data ada dalam firebase
                if(p0!!.exists()){
                    //mengabil setiap data dalam p0 => berisikan json objek
                    for(data in p0.children) // get key
                    { //hasil.add(data.key.toString())
                        //simpan hasil yang dibaca menjadi kelas dokter
                        val dokter = data.getValue(Dokter::class.java) as Dokter
                        //untuk hasil, yang disimpan hanya keyID dan id Dokter
                        dokter?.let { hasil.put(data.key.toString(),it.id) }
                    }
                }
            }
        })
        return hasil
    }

    //funsi untuk melakukan hapus data pada firebase.
    fun deleteData(id: Int) {
        //mengambil id utama dokter
        val key = dataTmp.filterValues { it== id }.keys
        ref = FirebaseDatabase.getInstance().getReference("DOCTERS")
        //remove value berdasarkan key
        ref.child(key.first()).removeValue()
        Toast.makeText(context,"Berhasil Dihapus!",Toast.LENGTH_SHORT).show()
        update()
    }

    //update pada dataTmp
    private fun update() {
        dataTmp = readData()
    }

    fun updateData(dataDokter: Dokter) {
        val key = dataTmp.filterValues { it== dataDokter.id}.keys
        ref = FirebaseDatabase.getInstance().getReference("DOCTERS")
        val dokterID = key.first()
        //membuat value untuk key untuk membentuk json objek dengan isi userData
        ref.child(dokterID).setValue(dataDokter).apply {
            addOnCompleteListener {//dijalankan jika penyimpanan data berhasil
                Toast.makeText(context,"Berhasil Update!",Toast.LENGTH_SHORT).show()
            }
            addOnFailureListener {//dijalankan jika penyimpanan data gagal
                Toast.makeText(context, "${it.message}", Toast.LENGTH_SHORT).show()
            }
        }
        update()
    }
}