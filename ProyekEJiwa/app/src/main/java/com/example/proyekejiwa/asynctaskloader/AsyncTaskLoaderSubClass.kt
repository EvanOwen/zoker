package com.example.proyekejiwa.asynctaskloader

import android.content.Context
import android.os.Bundle
import androidx.loader.content.AsyncTaskLoader
import com.example.proyekejiwa.asynctaskloader.db.myDBHelper
import kotlin.math.roundToInt

class AsyncTaskLoaderSubClass
    (context: Context, val args: Bundle?, val progressInterface: BackgroundProgressInterface, var mysqlitedb: myDBHelper?)
    : AsyncTaskLoader<ArrayList<Pasien>>(context) { //menggunakan parameter array agar returnya juga array list.

    //Permulaan Loading.
    override fun onStartLoading() {
        super.onStartLoading()
        forceLoad()
    }

    //Proses asyncronous.
    override fun loadInBackground(): ArrayList<Pasien> {

        if (mysqlitedb == null) {
            mysqlitedb = myDBHelper(context) //untuk membuat database, anda cukup memanggil perintah ini
        }
        //mengambil data dari database
        val dataArr = mysqlitedb?.getAllData()
        //inisialisasi array penampung semua data.
        val list = arrayListOf<Pasien>()
        //Mengambil data dari variabel penyimpan data dari database.
        for (i in dataArr!!.indices) {
            //mengubah data dalam bentuk data class
            val pasien = Pasien(
                dataArr[i].id,
                dataArr[i].nama,
                dataArr[i].jenisKelamin,
                dataArr[i].umur,
                dataArr[i].alamat,
                dataArr[i].noHp
            )
            //penambahan pada array penampung semua data.
            list.add(pasien)
            //karena data hanya sedikit, proses tidak akan keliahatan
            //sehingga dibuat Thread,sleep.
            Thread.sleep(300L)
            //melakukan update pada Interface
            progressInterface.onUpdateProgress(((i/dataArr.count().toFloat())*100).roundToInt())
        }
        return list  //result semua data.
    }
}