package com.example.proyekejiwa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.proyekejiwa.homeFragment.Companion.EXTRA_REPLAY
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.login_layout.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLoginSignIn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if(v?.id == R.id.btnLoginSignIn) {
            tvWrongLogin.visibility = View.VISIBLE
            //kondisi jika keduanya atau salah satu tidak diisi.
            if(tvUserName.text.toString().trim() == "" || tvPassword.text.toString().trim() == "") {
                tvWrongLogin.text = "Username dan Password Harus Diisi"
            }
            //kodisi jika username dan password benar.
            else if(tvUserName.text.toString().trim() == "zoker" && tvPassword.text.toString().trim() == "admin") {
                val replayData = Intent()
                replayData.putExtra(EXTRA_REPLAY, tvUserName.text.toString())
                setResult(RESULT_OK, replayData)
                finish()
            }
            //kodisi jika username salah dan password benar
            else if(tvUserName.text.toString().trim() != "zoker" && tvPassword.text.toString().trim() == "admin") {
                tvWrongLogin.text = "Username Salah"
            }
            //kodisi jika username benar dan password salah
            else if(tvUserName.text.toString().trim() == "zoker" && tvPassword.text.toString().trim() != "admin") {
                tvWrongLogin.text = "Password Salah"
            }
            //kodisi jika username dan password salah
            else {
                tvWrongLogin.text = "Username dan Password Salah"
            }
        }
    }
}
