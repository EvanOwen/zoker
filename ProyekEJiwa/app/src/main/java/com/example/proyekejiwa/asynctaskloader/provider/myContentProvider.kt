package com.example.proyekejiwa
import MyDB.pasienDB
import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.content.UriMatcher
import android.database.Cursor
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import com.example.proyekejiwa.asynctaskloader.Pasien
import com.example.proyekejiwa.asynctaskloader.db.myDBHelper

class myContentProvider : ContentProvider(){

    //uri untuk proses delete database.
    private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)

    private var dbHelper : myDBHelper?= null//mENGAKSES database

    override fun onCreate(): Boolean {
        dbHelper = myDBHelper(context as Context)
        return true //kembalikan true jika dbHelper berhasil dibuat, false jika gagal
    }
    //fungsi query digunakan untuk pengambilan data dari database
    override fun query(
        p0: Uri,
        p1: Array<String>?,
        p2: String?,
        p3: Array<String>?,
        p4: String?
    ): Cursor? {
        var queryBuilder = SQLiteQueryBuilder().apply {
            tables = USER_TABLE //menentukan table mana yang dapat anda bagikan
        }
        //Proses membaca database melalui dbhelper.?readableDatabase
        val cursor = queryBuilder.query(dbHelper?.readableDatabase,p1,p2,p3,null,null,p4)
        //memeberikan notifikasi pada resolver untuk menangkap data baru dari aplikasi ini
        cursor.setNotificationUri(context?.contentResolver, p0)
        return cursor
    }
    //untuk insert data
    override fun insert(p0: Uri, p1: ContentValues?): Uri? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    //untuk update data
    override fun update(p0: Uri, p1: ContentValues?, p2: String?, p3: Array<String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    //untuk delete data
    override fun delete(p0: Uri, p1: String?, p2: Array<String>?): Int {
        val deleted: Int = when (1) {
            sUriMatcher.match(p0) -> dbHelper!!.deleteById2(p0.lastPathSegment.toString())
            else -> 0
        }
        //memeberikan notifikasi pada resolver untuk menangkap perubahan
        context?.contentResolver?.notifyChange(CONTENT_URI, null)
        return deleted
    }
    //mendapatkan type.
    override fun getType(p0: Uri): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    //deklarsi variabel-variable;
    companion object{
        //nilai AUTHORITY
        val AUTHORITY = "com.example.com.example.proyekejiwa.asynctaskloader.provider.myContentProvider"
        //untuk mendapatkan table yang dapat diakses
        private val USER_TABLE = pasienDB.pasienTable.TABLE_PASIEN
        //deklarasi alamat uri
        val CONTENT_URI = Uri.parse("content://$AUTHORITY/$USER_TABLE")

    }
}
