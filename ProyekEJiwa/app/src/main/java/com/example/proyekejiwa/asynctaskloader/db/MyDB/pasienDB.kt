package MyDB

import android.provider.BaseColumns

//Menyimpan variabel-variabel yang digunakan di dalam database
object pasienDB {
    class pasienTable: BaseColumns {//digunakan untuk penamamaan database dalam pengenalan id "_ID"
        companion object {
            val TABLE_PASIEN = "pasien"
            val COLUMN_ID: String = "_id"
            val COLUMN_NAME: String = "nama"
            val COLUMN_JENISKELAMIN: String = "jenis_kelamin"
            val COLUMN_UMUR: String = "umur"
            val COLUMN_ALAMAT: String = "alamat"
            val COLUMN_TELEPON: String = "phone"
        }
    }
}
