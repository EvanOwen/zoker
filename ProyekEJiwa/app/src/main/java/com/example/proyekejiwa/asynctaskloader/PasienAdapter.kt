package com.example.proyekejiwa.asynctaskloader

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.proyekejiwa.R
import com.google.android.material.internal.ContextUtils.getActivity

class PasienAdapter(val context: Context,val listPasien: ArrayList<Pasien>) :
    RecyclerView.Adapter<PasienAdapter.GridViewHolder>() {

    private lateinit var onItemDeletedCallback: deleteItem
    private lateinit var onItemUpdatedCallback: updateItem

    fun setOnItemDeletedCallback(onItemDeletedCallback: deleteItem) {
        this.onItemDeletedCallback = onItemDeletedCallback
    }
    fun setOnItemUpdatedCallback(onItemUpdatedCallback: updateItem) {
        this.onItemUpdatedCallback = onItemUpdatedCallback
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): GridViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_pasien,
            viewGroup, false)
        return GridViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        holder.nama.text = listPasien[position].nama
        holder.jk.text = "Jenis Kelamin : " + listPasien[position].jenisKelamin
        holder.umur.text = "  Umur      : " + listPasien[position].umur.toString()
        holder.alamat.text = "Alamat     : " + listPasien[position].alamat
        holder.telp.text = "Telp          :  " +  listPasien[position].noHp
        val pasien = Pasien(
            0,
            listPasien[position].nama,
            listPasien[position].jenisKelamin,
            listPasien[position].umur,
            listPasien[position].alamat,
            listPasien[position].noHp
        )
        holder.btn_delete.setOnClickListener {
            onItemDeletedCallback.onItemDeleted(listPasien[position].id)
        }
        holder.btn_update.setOnClickListener {
            onItemUpdatedCallback.onItemUpdated(listPasien[position].id , pasien)
        }
    }

    override fun getItemCount(): Int {
        return listPasien.size
    }

    inner class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nama = itemView.findViewById<TextView>(R.id.tvName)
        var alamat = itemView.findViewById<TextView>(R.id.tvAlamat)
        var jk = itemView.findViewById<TextView>(R.id.tvJenisKelamin)
        var umur = itemView.findViewById<TextView>(R.id.tvUmur)
        var telp = itemView.findViewById<TextView>(R.id.tvTelp)
        var btn_delete = itemView.findViewById<Button>(R.id.btn_delete)
        var btn_update = itemView.findViewById<Button>(R.id.btn_update)
    }

    interface deleteItem {
        fun onItemDeleted(id: Int)
    }
    interface updateItem {
        fun onItemUpdated(id: Int, pasien:Pasien)
    }
}