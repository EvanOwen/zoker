package com.example.proyekejiwa.admod

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.proyekejiwa.R
import com.example.proyekejiwa.notification.PrefName
import kotlinx.android.synthetic.main.activity_setting_admod.*

class SettingAdmodActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_admod)

        //Mengambil nilai yang tersimpan di SharePreference admod
        val status = AdmodPrefHelper(this, PrefName).status_admod //mengambil nilai status_admod
        //jika status dalam keadaan true;
        if(status) {
            swAdmod.isChecked = true //mengubah switch menjadi on
            swAdmod.text = "admod aktif" //mengubah teks switch
        }
        swAdmod.setOnClickListener(this) //saat switch diklik.
    }

    override fun onClick(v: View?) {
        if(v?.id == R.id.swAdmod) {
            val admodPrefHelper = AdmodPrefHelper(this, PrefName)
            if(swAdmod.isChecked) {
                admodPrefHelper.status_admod = true //mengubah status pada shared preference.
            }else {
                swAdmod.text = "admod tidak aktif" //mengubah teks switch
                admodPrefHelper.status_admod = false //mengubah status pada shared preference.
            }
        }
    }
}

