package com.example.proyekejiwa.asynctaskloader

import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proyekejiwa.R
import kotlinx.android.synthetic.main.activity_note.*
import java.io.FileNotFoundException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.collections.ArrayList

class NoteActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var rviewNote : RecyclerView
    private lateinit var dataNote :ArrayList<Note>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)
        //penampung data dari penyimpanan internal
        dataNote = ArrayList()
        //pengambilan string data dari fungsi getData() dan mengubahnya menjadi array
        var dataArr = getData().split('\n')
        //Penimpanan data ke array penampung
        for(i in 0..dataArr.size-2 step 2) {
            dataNote.add(Note(dataArr[i], dataArr[i+1]))
        }

        //recyclerView
        rviewNote = findViewById(R.id.rvNote)
        rviewNote.setHasFixedSize(true)
        rviewNote.layoutManager = LinearLayoutManager(this)
        //memasukkan data ke adapter sehingga menghasilkan output.
        var noteAdapter = NoteAdapter(this, dataNote)
        rviewNote.adapter = noteAdapter

        //event Click
        btnAddNote.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        //penambahan Note
        if(v?.id == R.id.btnAddNote) {
            //deklarasi alert Dialog
            var alertDialog = AlertDialog.Builder(this)
            //layout untuk dialog
            var layoutNoteAlert = layoutInflater.inflate(R.layout.dialog_add_note_layout, null)
            var text_content = layoutNoteAlert.findViewById<EditText>(R.id.et_content)
            //mengatur view dari dialog
            alertDialog.setView(layoutNoteAlert)
            //Mengatur Positive button, agar data disave saat diklik
            alertDialog.setPositiveButton("Tambah", object : DialogInterface.OnClickListener {
                @RequiresApi(Build.VERSION_CODES.O)
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    saveData(text_content.text.toString()) //pemanggilan fungsi save data.
                    recreate() //melaukan refresh
                }
            })
            //mengatur Negative Button.
            alertDialog.setNegativeButton("Batal", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {

                }
            })

            var dialog = alertDialog.create()
            dialog.show() //menampilkan dialog.
        }
    }

    //Fungsi untuk menyimpan data ke penyimpanan Internal.
    @RequiresApi(Build.VERSION_CODES.O)
    fun saveData(textContent : String) {
        //menyimpan data sebelumnya, dengan mamanggil fungsi getData()
        var dataContent = getData()

        //Mengatur tanggal
        val current = LocalDateTime.now()
        val formatter = DateTimeFormatter.ISO_DATE
        val formatted = current.format(formatter)
        dataContent += formatted +'\n'+ textContent
        //Menulis ke file.
        var output = openFileOutput("Note.txt",
            Context.MODE_PRIVATE).apply {
            write(dataContent.toByteArray())
            close() //agar file tersimpan.
        }
    }

    //pengambilan data.
    fun getData():String {
        var dataText = "" //Penampung data dalam bentuk string
        try {
            var input = openFileInput("Note.txt")
                .bufferedReader().useLines {
                    //perulangan untuk mengambil data.
                    for (text in it.toList()) {
                        dataText += text + '\n';
                    }
                }
            //jika terjadi error
        } catch (e: FileNotFoundException) {
            Toast.makeText(this, "File Not Found", Toast.LENGTH_LONG).show()
        }
        return dataText //mengembalikan data.
    }
}
