package com.example.proyekejiwa.firebasecrud

import android.content.Context
import android.content.SharedPreferences

class idSharePrefHelper(context : Context, name : String) {
    //deklarasi variabel atau key yang akan disimpan dipreference
    val ID_DOKTER = "ID"
    private var myPreferences : SharedPreferences
    //Inisialisasi Preference;
    init{
        myPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }
    //menjalankan perintah penyimpanan
    inline fun SharedPreferences.editMe(operation : (SharedPreferences.Editor) -> Unit){
        val editMe = edit()
        operation(editMe)
        editMe.commit()
    }
    //Menyimpan dan mengambil nilai id_dokter di sharePrefence
    var id_dokter : Int
        get() = myPreferences.getInt(ID_DOKTER, 0)
        set(value){
            myPreferences.editMe {
                it.putInt(ID_DOKTER,value)
            }
        }
}