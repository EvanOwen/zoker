package com.example.proyekejiwa

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.proyekejiwa.admod.SettingAdmodActivity
import com.example.proyekejiwa.asynctaskloader.NoteActivity
import com.example.proyekejiwa.asynctaskloader.PasienActivity
import com.example.proyekejiwa.firebasecrud.DokterActivity
import com.example.proyekejiwa.notification.SettingNotificationActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)

        tvSettingNotif.setOnClickListener(this)
        tvSettingAdmod.setOnClickListener(this)
        tvPengggunaanApp.setOnClickListener(this)
        tvDataPasien.setOnClickListener(this)
        tvDataDokter.setOnClickListener(this)
        tvNotePasien.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if(v.id == R.id.tvSettingNotif) {
            startActivity(Intent(this, SettingNotificationActivity::class.java))
        }else if(v.id == R.id.tvDataPasien) {
            startActivity(Intent(this, PasienActivity::class.java))
        }else if(v.id == R.id.tvNotePasien) {
            startActivity(Intent(this, NoteActivity::class.java))
        }else if(v.id == R.id.tvDataDokter) {
            startActivity(Intent(this, DokterActivity::class.java))
        }else if(v.id == R.id.tvSettingAdmod) {
            startActivity(Intent(this, SettingAdmodActivity::class.java))
        }
    }
}
