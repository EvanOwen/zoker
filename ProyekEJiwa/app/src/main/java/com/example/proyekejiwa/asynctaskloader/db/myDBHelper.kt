package com.example.proyekejiwa.asynctaskloader.db
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

import MyDB.pasienDB
import MyDB.pasienDB.pasienTable.Companion.TABLE_PASIEN
import android.content.ContentValues
import android.database.Cursor
import android.database.SQLException
import android.provider.BaseColumns
import android.provider.BaseColumns._ID
import com.example.proyekejiwa.asynctaskloader.Pasien

class myDBHelper(context: Context):
    SQLiteOpenHelper(context, DATABASE_NAME,
        null,
        DATABASE_VERSION) {

    companion object{
        private val DATABASE_VERSION = 1 //deklarasi versi database yang digunakan.
        private val DATABASE_NAME = "mysqlitedb.db" //deklarasi nama database yang digunakan.
    }

    //Pembuatan tabel pada database
    override fun onCreate(p0: SQLiteDatabase?) {
        val CREATE_USER_TABLE = ("CREATE TABLE " +
                "${pasienDB.pasienTable.TABLE_PASIEN} " +
                "(${pasienDB.pasienTable.COLUMN_ID} INTEGER PRIMARY KEY AUTOINCREMENT," +
                "${pasienDB.pasienTable.COLUMN_NAME} TEXT," +
                "${pasienDB.pasienTable.COLUMN_JENISKELAMIN} TEXT," +
                "${pasienDB.pasienTable.COLUMN_UMUR} INTEGER," +
                "${pasienDB.pasienTable.COLUMN_ALAMAT} TEXT," +
                "${pasienDB.pasienTable.COLUMN_TELEPON} TEXT)")
        p0?.execSQL(CREATE_USER_TABLE)//perintah untuk menjalankan SQL
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0?.execSQL("DROP TABLE IF EXISTS ${pasienDB.pasienTable.TABLE_PASIEN}")
        onCreate(p0)
    }

    //Penambahan Data pada Database
    fun addPasien(pasien : Pasien) : Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(pasienDB.pasienTable.COLUMN_NAME,pasien.nama)
        contentValues.put(pasienDB.pasienTable.COLUMN_JENISKELAMIN,pasien.jenisKelamin)
        contentValues.put(pasienDB.pasienTable.COLUMN_UMUR,pasien.umur)
        contentValues.put(pasienDB.pasienTable.COLUMN_ALAMAT,pasien.alamat)
        contentValues.put(pasienDB.pasienTable.COLUMN_TELEPON,pasien.noHp)
        val success = db.insert(pasienDB.pasienTable.TABLE_PASIEN,
            null,
            contentValues) //perintah insert data pada sqlite
        db.close() //untuk memastikan askse database telah selesai
        return success //mengembalikan nilai, apakah gagal atau tidak.
    }

    //mengambil semua data dari database.
    fun getAllData() : List<Pasien>{
        val pasienList = ArrayList<Pasien>()
        val SELECT_ALL = "SELECT * " +
                "FROM ${pasienDB.pasienTable.TABLE_PASIEN}"//query select untuk mengambil semua data
        val db = this.readableDatabase //operasi untuk membaca data di database
        var cursor : Cursor? = null //cursor untuk membaca setiap baris data.
        try{
            cursor = db.rawQuery(SELECT_ALL,null)
        }catch (e:SQLException){
            //db.execSQL(SELECT_NAME) //jika gagal, maka akan mencoba fungsi execSQL
            return ArrayList()
        }
        var pasien:Pasien? //deklarasi variabel pasien
        if (cursor.moveToFirst()){ //moveToFirst agar cursor berada pada baris pertama
            do{
                var pasienId = cursor.getInt(
                    //cursor akan mengambil data berdasarkan id kolom pada baris cursor saat ini
                    cursor.getColumnIndex(pasienDB.pasienTable.COLUMN_ID)
                )
                var pasienNama = cursor.getString(
                    //cursor akan mengambil data berdasarkan nama kolom pada baris cursor saat ini
                    cursor.getColumnIndex(pasienDB.pasienTable.COLUMN_NAME)
                )
                var pasienJenisKelamin = cursor.getString(
                    //cursor akan mengambil data berdasarkan jenis kelamin kolom pada baris cursor saat ini
                    cursor.getColumnIndex(pasienDB.pasienTable.COLUMN_JENISKELAMIN)
                )
                var pasienUmur = cursor.getInt(
                    //cursor akan mengambil data berdasarkan umur kolom pada baris cursor saat ini
                    cursor.getColumnIndex(pasienDB.pasienTable.COLUMN_UMUR)
                )
                var pasienAlamat = cursor.getString(
                    //cursor akan mengambil data berdasarkan alamat kolom pada baris cursor saat ini
                    cursor.getColumnIndex(pasienDB.pasienTable.COLUMN_ALAMAT)
                )
                var pasienTelepon = cursor.getString(
                    //cursor akan mengambil data berdasarkan telepon kolom pada baris cursor saat ini
                    cursor.getColumnIndex(pasienDB.pasienTable.COLUMN_TELEPON)
                )
                //menampung  data dalam bentuk data class
                pasien = Pasien(pasienId,pasienNama, pasienJenisKelamin, pasienUmur, pasienAlamat, pasienTelepon)
                pasienList.add(pasien) //menyimpan data ke dalam array

                //nilai cursor.moveToNext() untuk berpindah ke baris berikutnya
            }while (cursor.moveToNext())
        }
        return pasienList
    }

    //Delete database
    fun deleteById(id: Int) {
        val db = this.writableDatabase
        db.delete(pasienDB.pasienTable.TABLE_PASIEN, pasienDB.pasienTable.COLUMN_ID +"=?",
            arrayOf(id.toString()))
        db.close()
    }
    //update database
    fun updateById(id: Int, pasien : Pasien) {
        val db = this.writableDatabase
        val contentValues = ContentValues() //Meletakkan semua text update
        contentValues.put(pasienDB.pasienTable.COLUMN_NAME,pasien.nama)
        contentValues.put(pasienDB.pasienTable.COLUMN_JENISKELAMIN,pasien.jenisKelamin)
        contentValues.put(pasienDB.pasienTable.COLUMN_UMUR,pasien.umur)
        contentValues.put(pasienDB.pasienTable.COLUMN_ALAMAT,pasien.alamat)
        contentValues.put(pasienDB.pasienTable.COLUMN_TELEPON,pasien.noHp)
        //menjalankan perintah update into versi sqlite, dengan mengirimkan
        val success = db.update(pasienDB.pasienTable.TABLE_PASIEN,
            contentValues,pasienDB.pasienTable.COLUMN_ID +"=?", arrayOf(id.toString()))
        db.close() //menutup database
    }

    fun deleteById2(id: String): Int {
        val db = this.writableDatabase
        return db.delete(TABLE_PASIEN, "$_ID = '$id'", null)
    }
}