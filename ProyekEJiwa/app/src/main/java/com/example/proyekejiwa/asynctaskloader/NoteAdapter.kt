package com.example.proyekejiwa.asynctaskloader

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.proyekejiwa.R


class NoteAdapter(val context: Context,val listNote: ArrayList<Note>) : RecyclerView.Adapter<NoteAdapter.GridViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): GridViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.note_layout, viewGroup, false)
        return GridViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        holder.tanggal.text = listNote[position].tanggal
        holder.content.text = listNote[position].content
        holder.btnReview.setOnClickListener{
            var intent = Intent(context, DetailNoteActivity::class.java)
            intent.putExtra("CONTENT", holder.content.text.toString())
            intent.putExtra("TANGGAL", holder.tanggal.text.toString())
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return listNote.size
    }

    inner class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tanggal = itemView.findViewById<TextView>(R.id.tv_tanggal_note)
        var content = itemView.findViewById<TextView>(R.id.tv_content_note)
        var btnReview = itemView.findViewById<Button>(R.id.btn_Review_note)
    }
}