package com.example.proyekejiwa.admod

import android.content.Context
import android.content.SharedPreferences

class AdmodPrefHelper(context : Context, name : String) {
    //deklarasi variabel atau key yang akan disimpan dipreference
    val STATUS = "STATUS"  //status untuk switch on atau tidak
    private var myPreferences : SharedPreferences
    //Inisialisasi Preference;
    init{
        myPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }
    //menjalankan perintah penyimpanan
    inline fun SharedPreferences.editMe(operation : (SharedPreferences.Editor) -> Unit){
        val editMe = edit()
        operation(editMe)
        editMe.commit()
    }
    //Menyimpan dan mengambil nilai status di sharePrefence
    var status_admod : Boolean
        //mengambil nilai
        get() = myPreferences.getBoolean(STATUS, true)
        //Menyimpan Nilai.
        set(value){
            myPreferences.editMe {
                it.putBoolean(STATUS,value)
            }
        }
}