package com.example.proyekejiwa.firebasecrud

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.proyekejiwa.R
import com.example.proyekejiwa.notification.PrefName
import com.example.proyekejiwa.sharedpreference.SharePrefHelper
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_dokter.*

class DokterActivity : AppCompatActivity(), View.OnClickListener
{
    //memdeklarasikan akses ke database controller
    lateinit var controller: DatabaseController
    //inisialisasi Recycle View
    private lateinit var rviewDokter : RecyclerView
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dokter)
        //inisialisasi recyclerView
        rviewDokter = findViewById(R.id.rvDokter)
        rviewDokter.setHasFixedSize(true)
        rviewDokter.layoutManager = LinearLayoutManager(this)
        controller = DatabaseController(this)
        //Pengambilan data dari firebase
        generateData()
        btnAddDokter.setOnClickListener(this)
    }

    private fun generateData() {
        val hasil = arrayListOf<Dokter>()
        val ref = FirebaseDatabase.getInstance().getReference("DOCTERS")
        //fungsi untuk membaca data
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented")
            }
            override fun onDataChange(p0: DataSnapshot) {
                //memastikan data ada dalam firebase
                if(p0!!.exists()){
                    //mengabil setiap data dalam p0 => berisikan json objek
                    for(data in p0.children) // get key
                    { //hasil.add(data.key.toString())
                        //simpan hasil yang dibaca menjadi data class dokter
                        val dokter = data.getValue(Dokter::class.java) as Dokter
                        //untuk hasil, yang disimpan hanya keyID dan username
                        hasil.add(dokter)
                    }
                    //memasukkan data ke adapter sehingga menghasilkan output.
                    val DokterAdapter = DokterAdapter(this@DokterActivity,hasil)
                    rviewDokter.adapter = DokterAdapter

                    //Tombol untuk menghapus data, dengan memanfaatkan interface dari pasien adapter
                    DokterAdapter.setOnItemDeletedCallback(object : DokterAdapter.deleteItemDokter {
                        override fun onItemDeleted(id: Int) {
                            AlertDialog.Builder(this@DokterActivity).apply {
                                setTitle("Konfirmasi")
                                setMessage("Apakah anda yakin data dihapus?")
                                    .setPositiveButton("YA",DialogInterface.OnClickListener {
                                            dialogInterface, i ->
                                        deleteData(id) //memanggil fungsi deleteData
                                    })
                                    .setNegativeButton("TIDAK",DialogInterface.OnClickListener {
                                            dialogInterface, i ->
                                        Toast.makeText(this@DokterActivity, "Tidak Jadi Dihapus",
                                            Toast.LENGTH_LONG).show()
                                    })
                            }.show()
                        }
                    })
                    //Tombol untuk menghapus data, dengan memanfaatkan interface dari pasien adapter
                    DokterAdapter.setOnItemUpdatedCallback(object : DokterAdapter.updateItemDokter {
                        override fun onItemUpdated(id: Int, dokter: Dokter) {
                            updateData(id, dokter) //memanggil fungsi updateData
                        }
                    })
                }else {
                    //Sehingga data yang tampil dihalaman juga kosong.
                    val DokterAdapter = DokterAdapter(this@DokterActivity,hasil)
                    rviewDokter.adapter = DokterAdapter
                }
            }
        })
    }

    override fun onClick(v: View?) {
        //dialog untuk memambah data.
        val dialogBuilt = AlertDialog.Builder(this)
        //layout untuk menambah data.
        val layoutAlert = layoutInflater.inflate(R.layout.dialog_add_dokter_layout, null)
        var jenis_kelamin = ""
        //deklarasi semua view.
        val nama = layoutAlert.findViewById<EditText>(R.id.tvNamaDokter)
        val jenis_kelamin_Lk = layoutAlert.findViewById<RadioButton>(R.id.rbJKLkDokter)
        val umur = layoutAlert.findViewById<EditText>(R.id.tvUmurDokter)
        val alamat = layoutAlert.findViewById<EditText>(R.id.tvAlamatDokter)
        val telepon = layoutAlert.findViewById<EditText>(R.id.tvTeleponDokter)

        //mengatur judul dan layout dari dialog
        dialogBuilt.setTitle("Input Data Dokter")
        dialogBuilt.setView(layoutAlert)

        //mengatur positive button.
        dialogBuilt.setPositiveButton("Tambah", object:DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if(jenis_kelamin_Lk.isChecked) {
                    jenis_kelamin = "Laki-laki"
                }else {
                    jenis_kelamin = "Perempuan"
                }
                //Save data ke firebase
                saveData(nama.text.toString(), umur.text.toString().toInt(),alamat.text.toString(),
                    jenis_kelamin, telepon.text.toString())
            }
        })
        //pengaturan Negative Button.
        dialogBuilt.setNegativeButton("Batal", object :DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
            }
        })
        val dialog = dialogBuilt.create()
        dialog.show() //Menampilkan dialog.
    }

    //Fungsi untuk menyimpan data ke database.
    fun saveData(nama: String, umur:Int, alamat:String, jenisKelamin:String, telepon:String) {
        val sharePrefHelper = idSharePrefHelper(this, "ID")
        //mengambil nilai terahir id dokter
        val id_dokter = sharePrefHelper.id_dokter
        sharePrefHelper.id_dokter += 1
        //mengubah data menjadi data class
        val dokter = Dokter(id_dokter,nama,jenisKelamin, umur, alamat, telepon);
        //memanggil function save pada Database Controller
        controller.saveData(dokter)
        generateData()
    }

    //funsi untuk melakukan delete terhadap data berdasarkan id
    fun deleteData(id : Int) {
        //memanggil fungsi delete pada database controller
        controller.deleteData(id)
        //melakukan refresh halaman.
        generateData()
    }

    fun updateData(id : Int, Dokter : Dokter) {
        //dialog untuk mengupdate data.
        val dialogBuilt = AlertDialog.Builder(this)
        //layout untuk mengupdate data.
        val layoutAlert = layoutInflater.inflate(R.layout.dialog_add_dokter_layout, null)
        var jenis_kelamin = ""
        //deklarasi semua view dari layoutAlert
        val nama = layoutAlert.findViewById<EditText>(R.id.tvNamaDokter)
        val jenis_kelamin_Lk = layoutAlert.findViewById<RadioButton>(R.id.rbJKLkDokter)
        val jenis_kelamin_Pr = layoutAlert.findViewById<RadioButton>(R.id.rbJKPrDokter)
        val umur = layoutAlert.findViewById<EditText>(R.id.tvUmurDokter)
        val alamat = layoutAlert.findViewById<EditText>(R.id.tvAlamatDokter)
        val telepon = layoutAlert.findViewById<EditText>(R.id.tvTeleponDokter)

        nama.setText(Dokter.nama)
        if(Dokter.jenisKelamin == "Laki-laki") jenis_kelamin_Lk.isChecked = true
        else jenis_kelamin_Pr.isChecked = true
        umur.setText(Dokter.umur.toString())
        alamat.setText(Dokter.alamat)
        telepon.setText(Dokter.noHp)

        //mengatur judul dan layout dari dialog
        dialogBuilt.setTitle("Update Data Dokter")
        dialogBuilt.setView(layoutAlert)

        //mengatur positive button.
        dialogBuilt.setPositiveButton("Update", object:DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if(jenis_kelamin_Lk.isChecked) {
                    jenis_kelamin = "Laki-laki"
                }else {
                    jenis_kelamin = "Perempuan"
                }
                //menyimpan data dalam bentuk data class yang akan dikirim ke DatabaseController
                val DokterTemp = Dokter(
                    id,
                    nama.text.toString(),
                    jenis_kelamin,
                    umur.text.toString().toInt(),
                    alamat.text.toString(),
                    telepon.text.toString()
                )
                //melakukan operasi update data ke firebase berdasarkan id
                controller.updateData(DokterTemp)
                generateData()
            }
        })
        //pengaturan Negative Button.
        dialogBuilt.setNegativeButton("Batal", object :DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
            }
        })
        val dialog = dialogBuilt.create()
        dialog.show() //Menampilkan dialog.
    }
}