package com.example.proyekejiwa.firebasecrud

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.proyekejiwa.R
import com.google.android.material.internal.ContextUtils.getActivity

class DokterAdapter(val context: Context, val listDokter: ArrayList<Dokter>) :
    RecyclerView.Adapter<DokterAdapter.GridViewHolder>() {

    private lateinit var onItemDeletedCallback: deleteItemDokter
    private lateinit var onItemUpdatedCallback: updateItemDokter

    fun setOnItemDeletedCallback(onItemDeletedCallback: deleteItemDokter) {
        this.onItemDeletedCallback = onItemDeletedCallback
    }
    fun setOnItemUpdatedCallback(onItemUpdatedCallback: updateItemDokter) {
        this.onItemUpdatedCallback = onItemUpdatedCallback
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): GridViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_dokter,
            viewGroup, false)
        return GridViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        holder.nama.text = listDokter[position].nama
        holder.jk.text = "Jenis Kelamin : " + listDokter[position].jenisKelamin
        holder.umur.text = "  Umur      : " + listDokter[position].umur.toString()
        holder.alamat.text = "Alamat     : " + listDokter[position].alamat
        holder.telp.text = "Telp          :  " +  listDokter[position].noHp
        val dokter = Dokter(
            0,
            listDokter[position].nama,
            listDokter[position].jenisKelamin,
            listDokter[position].umur,
            listDokter[position].alamat,
            listDokter[position].noHp
        )
        holder.btn_delete.setOnClickListener {
            onItemDeletedCallback.onItemDeleted(listDokter[position].id)
        }
        holder.btn_update.setOnClickListener {
            onItemUpdatedCallback.onItemUpdated(listDokter[position].id , dokter)
        }
    }

    override fun getItemCount(): Int {
        return listDokter.size
    }

    inner class GridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nama = itemView.findViewById<TextView>(R.id.tvName)
        var alamat = itemView.findViewById<TextView>(R.id.tvAlamat)
        var jk = itemView.findViewById<TextView>(R.id.tvJenisKelamin)
        var umur = itemView.findViewById<TextView>(R.id.tvUmur)
        var telp = itemView.findViewById<TextView>(R.id.tvTelp)
        var btn_delete = itemView.findViewById<Button>(R.id.btn_delete)
        var btn_update = itemView.findViewById<Button>(R.id.btn_update)
    }

    interface deleteItemDokter {
        fun onItemDeleted(id: Int)
    }
    interface updateItemDokter {
        fun onItemUpdated(id: Int, dokter:Dokter)
    }
}