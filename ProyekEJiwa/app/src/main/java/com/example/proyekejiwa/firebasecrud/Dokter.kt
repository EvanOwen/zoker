package com.example.proyekejiwa.firebasecrud

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Dokter(
    val id : Int = 0,
    val nama: String = "",
    val jenisKelamin: String = "",
    val umur : Int = 0,
    val alamat : String = "",
    val noHp : String = ""
) : Parcelable