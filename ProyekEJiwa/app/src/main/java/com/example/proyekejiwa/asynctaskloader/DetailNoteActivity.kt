package com.example.proyekejiwa.asynctaskloader

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.proyekejiwa.R
import kotlinx.android.synthetic.main.activity_detail_note.*

class DetailNoteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_note)
        var content = intent.getStringExtra("CONTENT")
        var tanggal = intent.getStringExtra("TANGGAL")
        tv_content_detail_note.text = content
        tv_tanggal_detail_note.text = tanggal
    }
}
