package com.example.proyekejiwa.sharedpreference

import android.content.Context
import android.content.SharedPreferences

class SharePrefHelper(context : Context, name : String) {
    //deklarasi variabel atau key yang akan disimpan dipreference
    val STATUS_ALARM = "STATUS"  //status untuk switch on atau tidak
    val TIME = "TIME" //menamppung string waktu
    private var myPreferences : SharedPreferences
    //Inisialisasi Preference;
    init{
        myPreferences = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }
    //menjalankan perintah penyimpanan
    inline fun SharedPreferences.editMe(operation : (SharedPreferences.Editor) -> Unit){
        val editMe = edit()
        operation(editMe)
        editMe.commit()
    }
    //Menyimpan dan mengambil nilai status di sharePrefence
    var status : Boolean
        get() = myPreferences.getBoolean(STATUS_ALARM, false)
        set(value){
            myPreferences.editMe {
                it.putBoolean(STATUS_ALARM,value)
            }
        }
    //Menyimpan dan mengambil nilai waktu di sharePrefence
    var time : String
        get() = myPreferences.getString(TIME,"NULL") as String
        set(value){
            myPreferences.editMe {
                it.putString(TIME,value)
            }
        }
    //Menghapus nilai pada sharePreference
    fun clearValues(){
        myPreferences.edit().clear().commit()
    }
}