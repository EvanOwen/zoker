package com.example.mysubapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pasien(
    val id : Int,
    val nama: String,
    val jenisKelamin: String,
    val umur : Int,
    val alamat : String,
    val noHp : String
) : Parcelable