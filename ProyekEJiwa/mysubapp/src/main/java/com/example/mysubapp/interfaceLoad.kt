package com.example.mysubapp

//The Interface
interface BackgroundProgressInterface {
    fun onUpdateProgress(progress: Int)
}