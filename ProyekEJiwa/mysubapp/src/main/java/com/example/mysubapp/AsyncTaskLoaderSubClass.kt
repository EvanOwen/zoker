package com.example.mysubapp

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.os.Bundle
import androidx.loader.content.AsyncTaskLoader
import com.example.mysubapp.pasienDB.pasienTable.Companion.COLUMN_ALAMAT
import com.example.mysubapp.pasienDB.pasienTable.Companion.COLUMN_ID
import com.example.mysubapp.pasienDB.pasienTable.Companion.COLUMN_JENISKELAMIN
import com.example.mysubapp.pasienDB.pasienTable.Companion.COLUMN_NAME
import com.example.mysubapp.pasienDB.pasienTable.Companion.COLUMN_TELEPON
import com.example.mysubapp.pasienDB.pasienTable.Companion.COLUMN_UMUR
import kotlin.math.roundToInt

class AsyncTaskLoaderSubClass
    (context: Context, val args: Bundle?, val progressInterface: BackgroundProgressInterface)
    : AsyncTaskLoader<ArrayList<Pasien>>(context) { //menggunakan parameter array agar returnya juga array list.

    private val myContentResolver : ContentResolver
    init {
        myContentResolver = context.contentResolver
    }
    //Permulaan Loading.
    override fun onStartLoading() {
        super.onStartLoading()
        forceLoad()
    }

    //Proses asyncronous.
    override fun loadInBackground(): ArrayList<Pasien> {
        //mengambil data dari database memalui content provider
        val dataArr = viewAllData();
        //inisialisasi array penampung semua data.
        val list = arrayListOf<Pasien>()
        //Mengambil data dari variabel penyimpan data dari database.
        for (i in dataArr!!.indices) {
            //mengubah data dalam bentuk data class
            val pasien = Pasien(
                dataArr[i].id,
                dataArr[i].nama,
                dataArr[i].jenisKelamin,
                dataArr[i].umur,
                dataArr[i].alamat,
                dataArr[i].noHp
            )
            //penambahan pada array penampung semua data.
            list.add(pasien)
            //karena data hanya sedikit, proses tidak akan keliahatan
            //sehingga dibuat Thread,sleep.
            Thread.sleep(300L)
            //melakukan update pada Interface
            progressInterface.onUpdateProgress(((i/dataArr.count().toFloat())*100).roundToInt())
        }
        return list  //result semua data.
    }

    fun viewAllData() : List<Pasien>{
        val pasienList : ArrayList<Pasien> = ArrayList()
        //untuk pembeda, proses ini menggunakan projection
        //untuk menentukan kolom data yang diambil
        val mProjection: Array<String> = arrayOf(
            COLUMN_NAME, COLUMN_JENISKELAMIN, COLUMN_UMUR,COLUMN_ALAMAT, COLUMN_TELEPON, COLUMN_ID
        )
        //berikutnya samaa seperti yang sudah di jelaskan sebelumnya
        var cursor1 : Cursor? = null
        cursor1 = myContentResolver.query(
            myContentProviderURI.CONTENT_URI,
            mProjection,null, null,null
        )
        if(cursor1!=null) {
            if (cursor1.moveToFirst()) {
                do {
                    var id = cursor1.getInt(cursor1.getColumnIndex(COLUMN_ID))
                    var name = cursor1.getString(cursor1.getColumnIndex(COLUMN_NAME))
                    var jenis_kelamin = cursor1.getString(cursor1.getColumnIndex(COLUMN_JENISKELAMIN))
                    var umur = cursor1.getInt(cursor1.getColumnIndex(COLUMN_UMUR))
                    var alamat = cursor1.getString(cursor1.getColumnIndex(COLUMN_ALAMAT))
                    var telepon = cursor1.getString(cursor1.getColumnIndex(COLUMN_TELEPON))
                    var pasien = Pasien(id, name, jenis_kelamin, umur, alamat, telepon);
                    pasienList.add(pasien)
                } while (cursor1.moveToNext())
            }
        }
        return pasienList
    }

}