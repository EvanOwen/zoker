package com.example.mysubapp

import android.net.Uri
import android.provider.BaseColumns

//Menyimpan variabel-variabel yang digunakan di dalam database
object pasienDB {
    const val AUTHORITY = "com.example.proyekejiwa.asynctaskloader.provider.myContentProvider"
    const val SCHEME = "content"

    class pasienTable: BaseColumns {//digunakan untuk penamamaan database dalam pengenalan id "_ID"
        companion object {
            val TABLE_PASIEN = "pasien"
            val COLUMN_ID: String = "_id"
            val COLUMN_NAME: String = "nama"
            val COLUMN_JENISKELAMIN: String = "jenis_kelamin"
            val COLUMN_UMUR: String = "umur"
            val COLUMN_ALAMAT: String = "alamat"
            val COLUMN_TELEPON: String = "phone"

            // Base content yang digunakan untuk akses content provider
            val CONTENT_URI: Uri = Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_PASIEN)
                .build()
        }
    }
}

class myContentProviderURI{
    companion object {
        val AUTHORITY = "com.example.proyekejiwa.asynctaskloader.provider.myContentProvider"
        private val USER_TABLE = pasienDB.pasienTable.TABLE_PASIEN
        val CONTENT_URI = Uri.parse("content://$AUTHORITY/$USER_TABLE")
    }
}